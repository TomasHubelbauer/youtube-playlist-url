# YouTube Playlist URL

**[Here's a tool for you!!](tool.html)**

- [ ] Create Chrome and Firefox extensions

YouTube, for some reason, make it super hard in the UI to get a URL that leads
to a playlist without also containing a cursor video.

This is the shitty URL that you get when you click on the *Playlists* tab of a
channel:

[`https://www.youtube.com/watch?v=nCKkHqlx9dE&list=PLGnWLXjIDnpBR4xqf3FO-xFFwE-ucq4Fj`](https://www.youtube.com/watch?v=nCKkHqlx9dE&list=PLGnWLXjIDnpBR4xqf3FO-xFFwE-ucq4Fj)

This link sucks, because clicking on it will make it play a video and the little
playlist box displayed to the right side is just pathetic for when it comes to
getting a glimpse of what is contained in the playlist.

YouTube also supports this concepts of shows/series which usually YouTube Red
channels take advantage of. It's displayed above the video title when on the
video page, and is clickable! It looks like this:

[`https://www.youtube.com/playlist?list=PLbIZ6k-SE9SiBhuk-ZPAe3UyxrIFjvC1q`](https://www.youtube.com/playlist?list=PLbIZ6k-SE9SiBhuk-ZPAe3UyxrIFjvC1q)

Hmm, okay, internally, these shows seems to be represented as playlists, they
even show up on the playlists page, but again, with the annoying cursor video
style URL! Well, working from that, will the above-title playlist detail style
URL work with any playlist ID?

**`https://www.youtube.com/playlist?list=` + the playlist ID**

Yes! Just append the part after `list=` from the shitty URL style and here you
go!
